import './style.css'
import * as THREE from 'https://unpkg.com/three@0.127.0/build/three.module.js'
const scene = new THREE.Scene()

//texture loader
const texLoader = new THREE.TextureLoader()
const photoMe = texLoader.load('photo.jpeg')
const textMe = texLoader.load('text.png')


//window updater

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}
window.addEventListener('resize', () => {
    //update size
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    //update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    //update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio,2))
})

const camera = new THREE.PerspectiveCamera( 75, sizes.width / sizes.height, 0.1, 1000 )

const aboutMe = document.querySelector('.aboutMe')
const back = document.querySelector('.back')
const header = document.querySelector('header')
function aboutMeCamera() {
    camera.position.setLength(5)
    if (aboutMe.style.display === 'none') {
        aboutMe.style.display = 'block'
        back.style.display = 'none'
        header.style.display = 'block'
    } else {
        aboutMe.style.display = 'none'
        back.style.display = 'block'
        header.style.display = 'none'
    }
}
function backCamera() {
    camera.position.setLength(30)
    if(back.style.display === 'block') {
        back.style.display = 'none'
        aboutMe.style.display = 'block'
        header.style.display = 'block'
    }
    else {
        back.style.display = 'block'
        aboutMe.style.display = 'none'
        header.style.display = 'block'
    }
}

aboutMe.onclick = aboutMeCamera
back.onclick = backCamera

const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#bg'),
})
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.setSize( sizes.width, sizes.height )
camera.position.setZ(30)


/**Object
 * 
*/
//Sphere1
const sphere1geometry = new THREE.SphereGeometry(15, 12, 6)
const sphere1material = new THREE.MeshStandardMaterial( { color: 0xaaaaaa, wireframe: true} )
const sphere1 = new THREE.Mesh( sphere1geometry, sphere1material )

//sphere2
const sphere2geo = new THREE.SphereGeometry(13, 12, 6)
const sphere2mat = new THREE.MeshNormalMaterial({flatShading: true, side: THREE.DoubleSide})
const sphere2 = new THREE.Mesh(sphere2geo, sphere2mat)
//star
const starGeo = new THREE.BufferGeometry()
const starCnt = 4000
const posArray = new Float32Array(starCnt * 3)

for(let i = 0; i < starCnt * 3; i++) {
    posArray[i] = (Math.random() - 0.5) * 75
}

starGeo.setAttribute('position', new THREE.BufferAttribute(posArray, 3))
const starMat = new THREE.PointsMaterial({
    size: 0.005
})
const star = new THREE.Points(starGeo, starMat)

//yourphoto
const photoGeo = new THREE.PlaneGeometry ( 3, 3 )
const photoMat = new THREE.MeshStandardMaterial({ color: 0xaaaaaa, side: THREE.DoubleSide, map: photoMe})
const photo = new THREE.Mesh( photoGeo, photoMat )
photo.position.x = -1.5

//your Profile
const textGeo = new THREE.PlaneGeometry(3, 5)
const textMat = new THREE.MeshStandardMaterial({color: 0x282929, side: THREE.DoubleSide, transparent:true, opacity: 1.5, map: textMe})
const text = new THREE.Mesh(textGeo,textMat)
text.position.x = 2.5

//aboutme group
const group = new THREE.Group()
group.add(text, photo)
//lighting
const ambientLight = new THREE.AmbientLight(0xffffff)

//add
scene.add(ambientLight, sphere1, sphere2, star, group)
const mouse = {
    x: undefined,
    y: undefined
}
function animate() {
    requestAnimationFrame(animate)
    sphere1.rotation.y += 0.001
    sphere1.rotation.z += 0.001
    sphere1.rotation.x += 0.001

    sphere2.rotation.y += 0.001
    sphere2.rotation.z += 0.001
    sphere2.rotation.x += 0.001

    star.rotation.y += -0.0011
    star.rotation.x += 0.0011
    star.rotation.z += -0.0011

    group.rotation.y = -mouse.x / 10
    group.rotation.x = mouse.y / 10
    renderer.render( scene, camera )
}
animate()

addEventListener ('mousemove', () => {
    mouse.x = (event.clientX/ innerWidth) * 2 - 1
    mouse.y = -(event.clientY/ innerHeight) * 2 + 1
    
    console.log(mouse)
})


